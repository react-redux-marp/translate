import React from 'react';
import LangContext from '../context/language_context.js';

class LanguageSelector extends React.Component {
    static contextType = LangContext;
    render(){
        return (
            <React.Fragment>
                <i className="flag us" onClick={() => this.context.onLangChange('english')}/>
                <i className="flag nl" onClick={() => this.context.onLangChange('dutch')}/>
            </React.Fragment>
        )
    }
}

export default LanguageSelector;