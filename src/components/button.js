import React from "react";
import LangContext from "../context/language_context.js";
import ColorContext from "../context/color_context.js";

class Button extends React.Component{
    render(){
        return (
            <ColorContext.Consumer>
            {(colorctx) => (
                <LangContext.Consumer>
                    {(ctx) => (
                        <button className={`btn-${colorctx.color} btn ${this.props.className}`}>
                            {ctx.lang == 'english'?"Submit":"Voorleggen"}
                        </button>
                    )}
                </LangContext.Consumer>
            )}
            </ColorContext.Consumer>
        );
    }
}

export default Button;