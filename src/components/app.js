import React from 'react';
import UserCreate from './user_create';
import ColorContext from '../context/color_context';
import LanguageSelector from './language_selector.js';
import {LangStore} from '../context/language_context.js';

class App extends React.Component{
    render(){
        return (
            <LangStore>
                <div className="container-fluid">
                    <div className="row">
                        <div className="col">
                            <h3>Select a language:</h3>
                            <LanguageSelector/>
                            <ColorContext.Provider value={{color:'success'}}>
                                <UserCreate/>
                            </ColorContext.Provider>
                        </div>
                    </div>
                </div>
            </LangStore>
        );
    }
}

export default App;