import React from "react";
import LangContext from "../context/language_context.js";

class Field extends React.Component{
    state={
        value:''
    }
    componentDidMount(){
        if(this.props.value){
            this.setState({value:this.props.value});
        }
    }
    handleChange = (ev) => {
        this.setState({value:ev.target.value});
    }
    render = () => {
        return (
            <LangContext.Consumer>
                {(context) => (
                <fieldset className={`field`}>
                    <label className="w-100">{context.lang == 'english'?"Name":"Naam"}</label>
                    <input
                        type="text"
                        className="w-100"
                        value={this.state.value}
                        onChange={this.handleChange}
                        placeholder={context.lang == 'english'?"Your input here...":"Uw inbreng hier ..."} />
                </fieldset>
                )}
            </LangContext.Consumer>
        );
    }
}

export default Field;