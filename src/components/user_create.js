import React from "react";
import Field from "./field.js";
import Button from "./button.js";
import langContext from "../context/language_context.js";

class UserCreate extends React.Component{
    static contextType = langContext;
    render(){
        return (
            <form id="user-create" className={`form ${this.props.className}`}>
                <Field/>
                <Button className="mt-3"/>
            </form>
        );
    }
}

export default UserCreate;