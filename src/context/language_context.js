import React from "react";

const defaultLang = {
    lang: 'english'
};

const LangContext = React.createContext({
    ...defaultLang
});

export class LangStore extends React.Component {
    state = {
        ...defaultLang
    }
    onLangChange = (lang) => {
        this.setState({lang});
    }
    render(){
        return (
            <LangContext.Provider value={{ ...this.state, onLangChange: this.onLangChange }}>
                {this.props.children}
            </LangContext.Provider>
        );
    }
}

export default LangContext;